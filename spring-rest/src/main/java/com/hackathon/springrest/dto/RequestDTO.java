package com.hackathon.springrest.dto;

public class RequestDTO {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RequestDTO(String message) {
        this.message = message;
    }
    public RequestDTO() {
    }
}
