package com.hackathon.springrest.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hackathon.springrest.dto.Documents;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class GetSentiment {
    String subscription_key = "4191eeff40b047adad513ff9cf093b91";
    String endpoint = "https://westcentralus.api.cognitive.microsoft.com";

    String path = "/text/analytics/v2.1";

    static Map<String, String[]> countMapData = new HashMap<>();
    static Map<String, Integer> scoreMap = new HashMap<>();
    static Map<String, List<Integer>> scoreMapList = new HashMap<>();

    public String getTextAnalytics(String addPath, Documents documents) throws Exception {
        String text = new Gson().toJson(documents);
        byte[] encoded_text = text.getBytes("UTF-8");

        URL url = new URL(endpoint + path + addPath);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "text/json");
        connection.setRequestProperty("Ocp-Apim-Subscription-Key", subscription_key);
        connection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.write(encoded_text, 0, encoded_text.length);
        wr.flush();
        wr.close();

        StringBuilder response = new StringBuilder();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            response.append(line);
        }
        in.close();

        return response.toString();
    }

    public static String prettify(String json_text) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(json_text).getAsJsonObject();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(json);
    }

    public void getCoutOfPhrase(String id, String response) {
        JSONObject jsonObject = new JSONObject(response);
        JSONArray array = jsonObject.getJSONArray("documents");
        JSONObject keyPhrase = (JSONObject) array.get(0);
        JSONArray keyPhraseArray = keyPhrase.getJSONArray("keyPhrases");


        for (int i = 0; i < keyPhraseArray.length(); i++) {
            String keyPhrasedata = keyPhraseArray.get(i).toString();
            if (countMapData.containsKey(keyPhrasedata)) {
                String[] valueArray = countMapData.get(keyPhrasedata);
                String[] valueArray1 = Arrays.copyOf(valueArray, valueArray.length + 1);
                valueArray1[valueArray1.length - 1] = id;
                countMapData.put(keyPhrasedata, valueArray1);
            } else {
                countMapData.put(keyPhrasedata, new String[]{id});
            }
        }

    }

    public void getAverageScore(String id, String response) {
        JSONObject jsonObject = new JSONObject(response);
        JSONArray array = jsonObject.getJSONArray("documents");
        JSONObject keyPhrase = (JSONObject) array.get(0);
        System.out.println(keyPhrase.get("score").toString());

        if (scoreMap.containsKey(id)) {
            double current = new BigDecimal(keyPhrase.get("score").toString()).doubleValue();
            Double doub = new Double(current*100);

            Integer mapData = scoreMap.get(id);
            Integer currentData = doub.intValue();
            scoreMap.put(id, (mapData+currentData) / 2);
        } else {
            double f1 = new BigDecimal(keyPhrase.get("score").toString()).doubleValue();
            Double doub = new Double(f1*100);
            scoreMap.put(id, doub.intValue());
        }

    }

    public void getAverageScoreV2(String id, String response) {
        JSONObject jsonObject = new JSONObject(response);
        JSONArray array = jsonObject.getJSONArray("documents");
        JSONObject keyPhrase = (JSONObject) array.get(0);
        System.out.println(keyPhrase.get("score").toString());

        if (scoreMapList.containsKey(id)) {
            double current = new BigDecimal(keyPhrase.get("score").toString()).doubleValue();
            Double doub = new Double(current*100);

            List<Integer> list = scoreMapList.get(id);
            Integer currentData = doub.intValue();
            list.add(currentData);
            scoreMapList.put(id, list);
        } else {
            double current = new BigDecimal(keyPhrase.get("score").toString()).doubleValue();
            Double doub = new Double(current*100);
            List<Integer> list = new ArrayList<>();
            list.add(doub.intValue());
            scoreMapList.put(id, list);
        }

    }


    public static Map<String, String[]> getCountMapData() {
        return countMapData;
    }

    public static Map<String, Integer> getScoreMap(String id) {
        Map<String, Integer> result = scoreMap.entrySet()
                .stream()
                .filter(map -> map.getKey().equals(id))
                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
        return result;
    }
    public static Map<String, Integer> getScoreMapV2(String id) {
        Map<String, Integer> result = scoreMapList.entrySet()
                .stream()
                .filter(map -> map.getKey().equals(id))
                .collect(Collectors.toMap(map -> map.getKey(),
                        map -> {Double data = map.getValue().stream().mapToInt(val -> val).average().orElse(0.0);
                                return  data.intValue();}));
        return result;
    }
    public static Map<String, Integer> getCurrentScore(String id) {
        Map<String, Integer> result = scoreMapList.entrySet()
                .stream()
                .filter(map -> map.getKey().equals(id))
                .collect(Collectors.toMap(map -> map.getKey(),
                        map -> map.getValue().get(map.getValue().size()-1)));
        return result;
    }

}