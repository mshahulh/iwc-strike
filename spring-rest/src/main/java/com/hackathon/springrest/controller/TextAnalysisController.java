package com.hackathon.springrest.controller;


import com.hackathon.springrest.dto.Documents;
import com.hackathon.springrest.dto.RequestDTO;
import com.hackathon.springrest.services.GetSentiment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@RestController
@RequestMapping("/textanalysis")
public class TextAnalysisController {

    @Autowired
    private GetSentiment obj;

    @CrossOrigin(origins ="*")
    @RequestMapping("/message/{id}")
    public Map getAnalytics(@PathVariable(name = "id") UUID id, @RequestBody RequestDTO req) {
        try {
            Map<String, Map> hashmap = new HashMap<>();
            String path = "/sentiment";
            Documents documents = new Documents();
            documents.add("1", "en", req.getMessage());

            String response = obj.getTextAnalytics(path, documents);
            System.out.println(GetSentiment.prettify(response));
            obj.getAverageScoreV2(id.toString(), response);
            Map averageMap = obj.getScoreMapV2(id.toString());
            hashmap.put("sentiment",averageMap);

//            path = "/languages";
//
//            String response1 = obj.getTextAnalytics(path, documents);
//            System.out.println(GetSentiment.prettify(response1));

            path = "/keyPhrases";

            String response2 = obj.getTextAnalytics(path, documents);
            obj.getCoutOfPhrase(id.toString(), response2);
            Map map = obj.getCountMapData();
            hashmap.put("keyPhrases",map);
            System.out.println(GetSentiment.prettify(response2));
            Map current = obj.getCurrentScore(id.toString());
            hashmap.put("Current-Sentimental-Score",current);
            return hashmap;

        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

}
