package com.hackathon.springrest.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/healthcheck")
public class HealthCheckController {

    @CrossOrigin(origins = "http://localhost:9002")
    @RequestMapping("/getdata")
    public String getHealthCheckData(){
        return "SUCCESS";
    }
}

